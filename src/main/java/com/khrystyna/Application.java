package com.khrystyna;

import com.khrystyna.views.GameView;
import com.khrystyna.views.View;

public class Application {
    public static void main(String[] args) {
        View view = new GameView();
        view.start();
    }
}
