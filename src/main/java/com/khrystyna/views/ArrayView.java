package com.khrystyna.views;

import com.khrystyna.model.arrays.ArraysService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class ArrayView implements View {

    private static Logger logger = LogManager.getLogger(ArrayView.class);
    private ArraysService arraysService = new ArraysService();

    @Override
    public void start() {
        logger.info("Hi there!");
        int[] a = {1, 2, 3, 4, 5};
        int[] b = {3, 4, 5, 6, 7, 8};
        int[] c = {1, 1, 2, 2, 3, 4, 5, 4, 5, 4, 4, 5, 5, 6, 1, 2, 3};

        logger.info(Arrays
                .toString(arraysService.getCommon(a, b)));
        logger.info(Arrays
                .toString(arraysService.getDifferent(a, b)));
        logger.info(Arrays
                .toString(arraysService.getRepeatedNoMoreThanTwice(c)));
        logger.info(Arrays
                .toString(arraysService.removeCopies(c)));
    }

}
