package com.khrystyna.views;

public interface View {
    void start();
}
