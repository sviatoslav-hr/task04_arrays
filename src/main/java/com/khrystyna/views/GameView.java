package com.khrystyna.views;

import com.khrystyna.controllers.GameController;
import com.khrystyna.model.game.GameStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class GameView implements View {
    private static Logger logger = LogManager.getLogger(GameView.class);

    private GameController controller = GameController.getInstance();
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void start() {
        logger.trace("Starting game...");
        controller.startGame();

        runGameCycle();

        System.out.print("Play again?(Y/N): ");
        if (getCharChoice() == 'Y') {
            start();
        }
    }

    private void runGameCycle() {
        String answer;
        printDoors();
        printDeadlyDoorsNumber();

        while (controller.getGameStatus() == GameStatus.PLAYING) {
            answer = controller.openDoor(getIntChoice());
            printDoors();
            printDeadlyDoorsNumber();
            logger.info(answer);
            logger.info(controller.getPlayer());
        }

        if (controller.getGameStatus() == GameStatus.LOSE) {
            logger.info(controller.getWinOrder());
        } else if (controller.getGameStatus() == GameStatus.WIN) {
            logger.info("Congratulations, you won!!!");
        }
    }

    private void printDeadlyDoorsNumber() {
        if (controller.getGameStatus() == GameStatus.PLAYING) {
            logger.info("Monsters you aren\'t able to beat are behind " +
                    controller.getDeadlyDoorsNumber() + " doors");
        }
    }

    private void printDoors() {
        logger.info(controller.getHallDoorsAsString());
    }

    private int getIntChoice() {
        System.out.print("Type door number: ");
        return scanner.nextInt();
    }

    private char getCharChoice() {
        return scanner.next()
                .trim().toUpperCase().charAt(0);
    }
}
