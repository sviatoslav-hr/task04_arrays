package com.khrystyna.controllers;

import com.khrystyna.model.game.GameStatus;
import com.khrystyna.model.game.Hall;
import com.khrystyna.model.game.Player;

public class GameController {
    private static GameController instance = new GameController();

    public static GameController getInstance() {
        return instance;
    }

    private Hall hall;
    private GameStatus gameStatus = GameStatus.NOT_STARTED;

    public void startGame() {
        hall = new Hall(new Player());
        gameStatus = GameStatus.PLAYING;
    }

    public String getHallDoorsAsString() {
        return hall.getDoorsAsString();
    }

    public String openDoor(int doorNumber) {
        String answer = hall.openDoor(doorNumber);
        if (!hall.getPlayer().isAlive()) {
            gameStatus = GameStatus.LOSE;
        } else if (hall.getUnopenedDoorsNumber() == 0) {
            gameStatus = GameStatus.WIN;
        }

        return answer;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public String getPlayer() {
        return hall.getPlayer().toString();
    }

    public int getDeadlyDoorsNumber() {
        return hall.countDeadlyDoors();
    }

    public String getWinOrder() {
        return hall.foundWinOrder();
    }
}
