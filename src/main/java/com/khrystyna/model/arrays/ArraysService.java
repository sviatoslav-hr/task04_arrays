package com.khrystyna.model.arrays;

public class ArraysService {
    /**
     * Returns array containing common elements of two arrays.
     *
     * @param a first array
     * @param b second array
     * @return array that contains common elements
     */
    public int[] getCommon(final int[] a, final int[] b) {
        int arrayLength = 0;

        for (int aItem : a) {
            for (int bItem : b) {
                if (aItem == bItem) {
                    arrayLength++;
                }
            }
        }
        int[] result = new int[arrayLength];

        for (int i = 0, k = 0; i < a.length; i++) {
            for (int value : b) {
                if (a[i] == value) {
                    result[k] = a[i];
                    k++;
                }
            }
        }
        return result;
    }

    /**
     * Returns array containing different elements of array a
     * comparing to array b.
     *
     * @param a first array
     * @param b second array
     * @return array that contains different elements of array a
     * comparing to array b
     */
    public int[] getDifferent(final int[] a, final int[] b) {
        int arrayLength = 0;

        outer:
        for (int aItem : a) {
            for (int bItem : b) {
                if (aItem == bItem) {
                    continue outer;
                }
            }
            arrayLength++;
        }
        int[] result = new int[arrayLength];

        outer:
        for (int i = 0, k = 0; i < a.length; i++) {
            for (int value : b) {
                if (a[i] == value) {
                    continue outer;
                }
            }
            result[k] = a[i];
            k++;
        }
        return result;
    }

    public int[] getRepeatedNoMoreThanTwice(int[] array) {
        int length = array.length;
        outer:
        for (int i = 0; i < array.length; i++) {
            int equals = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j] && j >= i) {
                    equals++;
                } else if (array[i] == array[j] && j < i) {
                    continue outer;
                }
            }
            if (equals > 2) {
                length -= equals;
            }
        }

        int[] result = new int[length];
        for (int i = 0, k = 0; i < array.length; i++) {
            int equals = 0;
            for (int value : array) {
                if (array[i] == value) {
                    equals++;
                }
            }
            if (equals <= 2) {
                result[k] = array[i];
                k++;
            }
        }
        return result;
    }

    public int[] removeCopies(int[] a) {
        int length = a.length;
        for (int i = 0; i < a.length; i++) {
            if (i + 1 < a.length && a[i] == a[i + 1]) {
                length--;
            }
        }
        int[] result = new int[length];

        for (int i = 0, k = 0; i < a.length; i++) {
            if (i + 1 >= a.length || a[i] != a[i + 1]) {
                if (k - 1 >= 0 && a[i] == result[k - 1]) {
                    continue;
                }
                result[k] = a[i];
                k++;
            }
        }
        return result;

    }
}
