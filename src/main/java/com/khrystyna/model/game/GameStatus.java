package com.khrystyna.model.game;

public enum GameStatus {
    NOT_STARTED, PLAYING, LOSE, WIN
}
