package com.khrystyna.model.game;

public class Player extends Entity {
    private static final int START_POWER = 25;

    public Player() {
        super(START_POWER);
    }

    void useArtifact(Artifact artifact) {
        setPower(getPower() + artifact.getPowerBuff());
    }

    void fightAgainstMonster(Monster monster) {
        int power = getPower() - monster.getPower();
        setPower(power < 0 ? 0 : power);
        if (getPower() > 0) {
            monster.setAlive(false);
        } else {
            setAlive(false);
        }
    }

    @Override
    public String toString() {
        return "Player [" + getPower() + " POWER]";
    }
}
