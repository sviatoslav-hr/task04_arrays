package com.khrystyna.model.game;

class Door {
    private DoorInsideEntityType entityType;
    private BehindDoor entity;
    private boolean isOpen = false;

    DoorInsideEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(DoorInsideEntityType entityType) {
        this.entityType = entityType;
    }

    BehindDoor getEntity() {
        return entity;
    }

    void setEntity(BehindDoor entity) {
        this.entity = entity;
    }

    boolean isOpen() {
        return isOpen;
    }

    void setOpen() {
        isOpen = true;
    }
}
