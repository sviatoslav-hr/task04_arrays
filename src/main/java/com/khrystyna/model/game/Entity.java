package com.khrystyna.model.game;

public abstract class Entity {
    private int power;
    private boolean isAlive = true;

    Entity(int power) {
        this.power = power;
    }

    int getPower() {
        return power;
    }

    void setPower(int power) {
        this.power = power;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }
}
