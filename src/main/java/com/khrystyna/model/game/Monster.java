package com.khrystyna.model.game;

class Monster extends Entity implements BehindDoor {
    static final int MIN_POWER = 5;
    static final int MAX_POWER = 100;

    Monster(int power) {
        super(power);
    }

    @Override
    public String toString() {
        return "Monster [" + getPower() + " POWER]";
    }
}
