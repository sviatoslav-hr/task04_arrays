package com.khrystyna.model.game;

public enum DoorInsideEntityType {
    MONSTER, ARTIFACT
}
