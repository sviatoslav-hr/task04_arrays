package com.khrystyna.model.game;

public class Artifact implements BehindDoor {
    public static final int MIN_POWER_BUFF = 10;
    public static final int MAX_POWER_BUFF = 80;

    private int powerBuff;

    public Artifact(int powerBuff) {
        if (powerBuff < MIN_POWER_BUFF || powerBuff > MAX_POWER_BUFF) {
            throw new RuntimeException("Incorrect powerBuff value");
        }
        this.powerBuff = powerBuff;
    }

    public int getPowerBuff() {
        return powerBuff;
    }

    @Override
    public String toString() {
        return "Artifact{ " +
                "+" + powerBuff +
                "P }";
    }
}
