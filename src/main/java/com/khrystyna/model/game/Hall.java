package com.khrystyna.model.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Hall {
    private static Logger logger = LogManager.getLogger(Hall.class);
    private final int DOORS_NUMBER = 10;
    private Door[] doors = new Door[DOORS_NUMBER];
    private int unopenedDoors;
    private Player player;

    public Hall(Player player) {
        this.player = player;
        init();
    }

    public Player getPlayer() {
        return player;
    }

    public int getUnopenedDoorsNumber() {
        return unopenedDoors;
    }

    private void init() {
        Random random = new Random();

        for (int i = 0; i < doors.length; i++) {
            boolean isMonster = random.nextBoolean();
            Door door = new Door();
            if (isMonster) {
                door.setEntityType(DoorInsideEntityType.MONSTER);
                door.setEntity(
                        new Monster(Monster.MIN_POWER
                                + random.nextInt(Monster.MAX_POWER
                                - Monster.MIN_POWER)));
            } else {
                door.setEntityType(DoorInsideEntityType.ARTIFACT);
                door.setEntity(
                        new Artifact(Artifact.MIN_POWER_BUFF
                                + random.nextInt(Artifact.MAX_POWER_BUFF
                                - Artifact.MIN_POWER_BUFF)));
            }
            doors[i] = door;
        }
        unopenedDoors = doors.length;
    }

    public String getDoorsAsString() {
        StringBuilder string = new StringBuilder();

        for (int i = 0; i < doors.length; i++) {
            string.append("\n--------------------------\n")
                    .append("Door #")
                    .append(i + 1)
                    .append(": ");
            if (doors[i].isOpen()) {
                string.append(doors[i].getEntity());
            } else {
                string.append('?');
            }
            string.append("\n--------------------------");
        }
        return string.toString();
    }

    public String openDoor(int doorNumber) {
        if (doorNumber >= doors.length || doorNumber <= 0) {
            logger.warn("Incorrect door number!");
        }

        Door door = doors[doorNumber - 1];

        if (door.isOpen()) {
            logger.warn("Door is already open!");
        } else {
            unopenedDoors--;
            return goInside(door);
        }
        return null;
    }

    private String goInside(Door door) {
        door.setOpen();
        if (door.getEntityType() == DoorInsideEntityType.ARTIFACT) {
            Artifact artifact = (Artifact) door.getEntity();
            player.useArtifact(artifact);
            return "You found artifact that gave you "
                    + artifact.getPowerBuff() + " power!";
        } else if (door.getEntityType() == DoorInsideEntityType.MONSTER) {
            player.fightAgainstMonster((Monster) door.getEntity());
            if (player.isAlive()) {
                return "You killed monster!";
            } else {
                return "You are dead...";
            }
        }
        return null;
    }

    public int countDeadlyDoors() {
        return countDeadlyDoorsFrom(0, 0);
    }

    private int countDeadlyDoorsFrom(int doorIndex, int found) {
        Door door = doors[doorIndex];
        if (doorIndex < doors.length - 1) {
            if (!door.isOpen()
                    && door.getEntityType() == DoorInsideEntityType.MONSTER
                    && ((Monster) door.getEntity())
                    .getPower() > player.getPower()) {
                found++;
            }
            found = countDeadlyDoorsFrom(doorIndex + 1, found);
        }
        return found;
    }

    public String foundWinOrder() {
        int maxBuff = 0;
        int maxDebuff = 0;
        StringBuilder artifactsOrder = new StringBuilder();
        StringBuilder monstersOrder = new StringBuilder();
        for (int i = 0; i < doors.length; i++) {
            Door door = doors[i];
            if (door.getEntityType() == DoorInsideEntityType.ARTIFACT) {
                maxBuff += ((Artifact) door.getEntity()).getPowerBuff();
                artifactsOrder.append(i + 1).append(" ");
            } else if (door.getEntityType() == DoorInsideEntityType.MONSTER) {
                maxDebuff += ((Monster) door.getEntity()).getPower();
                monstersOrder.append(i + 1).append(" ");
            }
        }
        if (maxBuff < maxDebuff) {
            return "There was no way to win...";
        } else {
            return artifactsOrder.append(monstersOrder)
                    .insert(0, "Win order: ").toString();
        }
    }
}


